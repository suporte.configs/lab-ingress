## Lab ingress Controller - bate-metal

### Urls relacionadas
https://kubernetes.github.io/ingress-nginx/deploy/#bare-metal
https://github.com/kubernetes/ingress-nginx/
https://kubernetes.io/docs/concepts/services-networking/ingress/

### Clonar repositorio ingress-nginx
```
git clone https://github.com/kubernetes/ingress-nginx.git
```

### Install Ingress-Controlller
```
# cd ingress-nginx/deploy/static/provider/baremetal/
# vim deploy.yaml

spec:
  type: NodePort
  ports:
    - name: http
      port: 80
      protocol: TCP
      targetPort: http
      appProtocol: http
    - name: https
      port: 443
      protocol: TCP
      targetPort: https
      appProtocol: https
  selector:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/component: controller
  externalIPs:  ## add
    - 172.27.11.10  ## add ip master


# kubectl create -f deploy.yaml

```
### Adicionado ao hosts apontamento
```
echo "172.27.11.10 lab.k8s.gandhiva.io" > /etc/hosts
``` 

### Teste Access
```
 curl http://lab.k8s.gandhiva.io/
<html>
<head><title>404 Not Found</title></head>
<body>
<center><h1>404 Not Found</h1></center>
<hr><center>nginx</center>
</body>
</html>
```
### Create lab-ingress
```
kubectl create -f lab-ingress.yaml
```

### Craate ingress file - app1 app2
```
kubectl create -f simple-ingress.yaml

apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: simple-api-ing
  namespace: lab-ingress
  annotations:
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - host: lab.k8s.gandhiva.io
    http:
      paths:
      - path: "/app1"
        pathType: Prefix
        backend:
          service:
            name: srv-app1
            port:
              number: 80
      - path: "/app2"
        pathType: Prefix
        backend:
          service:
            name: srv-app2
            port:
              number: 80

```

### Teste acesso app1
```
 curl http://lab.k8s.gandhiva.io/app1

....
<br></p>

<h1 id="toc_0">Hello Raissa!</h1>

<p>This is being served from a <b>docker</b><br>
container running Nginx.</p>


</body>

</html>

```

### Teste acesso app1
```
 curl http://lab.k8s.gandhiva.io/app2

....
<br></p>

<h1 id="toc_0">Hello Maria Clara!</h1>

<p>This is being served from a <b>docker</b><br>
container running Nginx.</p>


</body>

</html>

```
